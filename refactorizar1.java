/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

import java.*;
import java.util.*;
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class refac {
	final static String cad = "Bienvenido al programa";
	public static void main(String[] args) {
		int a, b;
		String cadena1, cadena2;
		Scanner c = new Scanner(System.in);
		
		System.out.println(cad);
		System.out.println("Introduce tu dni");
		cadena1 = c.nextLine();
		System.out.println("Introduce tu nombre");
		cadena2 = c.nextLine();
		
		a=7; b=16;
		int numeroc = 25;
		if(a>b||numeroc%5!=0&&(numeroc*3-1)>b/numeroc)
		{
			System.out.println("Se cumple la condición");
		}
		
		numeroc = a+b*numeroc+b/a;
		
		String array[] = new String[7];
		array[0] = "Lunes";
		array[1] = "Martes";
		array[2] = "Miercoles";
		array[3] = "Jueves";
		array[4] = "Viernes";
		array[5] = "Sabado";
		array[6] = "Domingo";
		
		recorrer_array(array);
	}
	static void recorrer_array(String vectordestrings[])
	{
		for(int dia=0;dia<7;dia++) 
		{
			System.out.println("El dia de la semana en el que te encuentras ["+(dia+1)+"-7] es el dia: "+vectordestrings[dia]);
		}
	}
	
}
